from rest_framework import serializers
from locutors.models import Locutor, UsesLanguage
from upload_batches.models import UploadBatch


class UsesLanguageSerializer(serializers.ModelSerializer):
    def create(self, validated_data):
        # Apply the locutor from the context that's provided by the view to the serializer
        validated_data['locutor'] = self.context['locutor']
        return UsesLanguage.objects.create(**validated_data)

    class Meta:
        model = UsesLanguage
        exclude = ['id', 'locutor'] # all fields except id and locutor (avoid redundancy)


class LocutorSerializer(serializers.ModelSerializer):
    languages_used = UsesLanguageSerializer(many=True, read_only=True)
    can_be_deleted = serializers.SerializerMethodField()

    class Meta:
        model = Locutor
        fields = "__all__"

    def get_can_be_deleted(self, obj):
        return obj.can_be_deleted
