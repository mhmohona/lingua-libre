# Lingua Libre

[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=lingua-libre_lingua-libre&metric=code_smells)](https://sonarcloud.io/summary/new_code?id=lingua-libre_lingua-libre)
[![Technical Debt](https://sonarcloud.io/api/project_badges/measure?project=lingua-libre_lingua-libre&metric=sqale_index)](https://sonarcloud.io/summary/new_code?id=lingua-libre_lingua-libre)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=lingua-libre_lingua-libre&metric=coverage)](https://sonarcloud.io/summary/new_code?id=lingua-libre_lingua-libre)
[![Lines of Code](https://sonarcloud.io/api/project_badges/measure?project=lingua-libre_lingua-libre&metric=ncloc)](https://sonarcloud.io/summary/new_code?id=lingua-libre_lingua-libre)

**Lingua Libre** is an online collaborative project and tool, which aims to build a collaborative, multilingual, audiovisual speech corpus under a free license ([English Wikipedia](https://en.wikipedia.org/wiki/Lingua_Libre)).

This repository contains both the backend (Django) and frontend (Vue) code of the Lingua Libre website.

## Technical documentation
Technical documentation (architecture, database models, API routes) is available in the [`doc/`](doc) folder.
It will help you grasp all the various concepts that pertain to Lingua Libre's inner workings.

The backend exposes a public REST API that is used by the frontend and that you can also use for your project.
The REST API definition is available at [`doc/openapi.yaml`](doc/openapi.yaml).

## Installation
This tutorial covers all the steps required to install and run Lingua Libre, but is geared towards using this installation as a local development environment.

**If you wish to deploy Lingua Libre in a production environement**, we maintain Ansible playbooks in the [`operations` repository](https://gitlab.wikimedia.org/repos/wikimedia-france/lingua-libre/operations).
You can either use them with Ansible, or read through them as some sort of step-by-step tutorials to deploy and/or update Lingua Libre.

### Requirements
* Python 3.10.x
* Node 18+
* MariaDB
  * Run `apt install build-essential default-libmysqlclient-dev pkg-config` to ensure you have the necessary dependencies for the MariaDB Django connector

### Create the MariaDB database
1. Create a database (default: `lingualibre`)
2. Create a MariaDB user (default: `lingualibre@localhost`)
3. Grant all privileges on this database to the user

### `config.ini`
1. `cp config.ini.sample config.ini`
2. Add the OAuth key and secret
3. Configure the database settings (host, port, name, user, password) with the credentials you created for the MariaDB database.
4. (if serving the website on the web) change the Django secret key.

### Install
1. `python -m venv .venv`
2. `source .venv/bin/activate`
3. `pip install -r requirements.txt`
4. `python manage.py migrate`
5. `cd front-end/ && npm clean-install`

### Launch!
Make sure ports 8080 and 8079 are free.

Run `python manage.py runserver 8080` and `cd front-end/ && npm run dev` !
(Ports are currently hardcoded in the code).

## Support
Found a bug?
Please report it on our issue tracker, so we can squash it as soon as possible!

Want a new feature?
We recommend you talk about this feature first on our discussion spaces.

Issue tracker: [phabricator.wikimedia.org/project/view/6913/](https://phabricator.wikimedia.org/project/view/6913/).

## Contribute
### Write code
#### Front-end
When contributing code to the Vue front-end, do remember to run the following commands, as they automatically apply the code style guidelines and make sure your code does not introduce any new issues:

```
npm run format    # Apply the code style guidelines
npm run lint:fix  # Run the linter and fix issues that can be automatically fixed
npm run test:unit # Run unit tests
```

Writing unit tests for Components you created is *highly* recommended. Ideally, the front-end should have 100% coverage at some point.

The front-end currently does not have integration (or end-to-end) tests.
If you have any knowledge in designing or writing such tests, please reach out!

### Run tests
#### Back-end
Give the db user all privileges on the `test_lingualibre` (or a database where you prepend `test_` to your db's name) database (do not create it).

(in the venv, FOR COVERAGE REPORT)
1. `coverage erase`
2. `coverage run manage.py test`
3. `coverage report`

Otherwise
`python manage.py test`

## License
This project is licensed under AGPL v3 by Wikimédia France.

### Licenses of used media
* [Lingua Libre logo no text](https://commons.wikimedia.org/wiki/File:Lingualibre-logo-no-text.svg), CC BY-SA 4.0 Marc Brouillon, Keymap9, 0x010C.
* [Wikimedia France logo - horizontal](https://commons.wikimedia.org/wiki/File:Wikimedia_France_logo_-_horizontal.svg), CC BY-SA 4.0 Ash Crow, Wikimedia Foundation Trademark Policy.

## Project status
Lingua Libre development is currently funded by the DGLFLF (Délégation Générale à la Langue Française et aux Langues de France), a unit of the French Ministry of Culture.
Wikimédia France hired Florian Cuny from October 2023 to January 2024.

As of 2024-01-04, Florian Cuny's contract ended and development to Lingua Libre became volunteer-only.
