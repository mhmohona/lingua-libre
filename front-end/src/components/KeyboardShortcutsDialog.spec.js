import { describe, it, expect, afterEach } from 'vitest'
import { mount } from '@vue/test-utils'
import KeyboardShortcutsDialog from './KeyboardShortcutsDialog.vue'

describe('Button to open the dialog', () => {
  afterEach(() => {
    // We need to manually clean up the body HTML code between unit tests
    // due to CdxDialog's use of <Teleport>
    document.body.outerHTML = ''
  })

  it('should be there', () => {
    const wrapper = mount(KeyboardShortcutsDialog, { props: { shortcuts: [] } })
    expect(wrapper.find('button')).toBeTruthy()
  })

  it('should open the dialog when clicked', async () => {
    const wrapper = mount(KeyboardShortcutsDialog, {
      props: { shortcuts: [{ key: 'F', description: 'Pay Respects', long: false }] },
    })

    // no kbd elements should be visible at that point - i.e. in the DOM
    // CdxDialog uses Vue's Teleport, so we need to check for tags in the document's body.
    // wrapper has no idea where the dialog actually is.
    expect(document.body.getElementsByTagName('kbd').length).toBe(0)

    await wrapper.getComponent('button').trigger('click')

    // there should be at least one <kbd> in the DOM now
    expect(document.body.getElementsByTagName('kbd').length).toBeGreaterThan(0)
  })
})

describe('Dialog', () => {
  afterEach(() => {
    document.body.outerHTML = ''
  })

  it('should display the key name inside <kbd>', async () => {
    const wrapper = mount(KeyboardShortcutsDialog, {
      props: {
        shortcuts: [{ key: 'F', description: 'Pay Respects', long: false }],
      },
    })

    // show the dialog
    await wrapper.find('button').trigger('click')

    expect(document.body.getElementsByTagName('kbd')[0].innerHTML).toStrictEqual('F')
  })

  it('should have the right amount of <kbd> tags', async () => {
    const wrapper = mount(KeyboardShortcutsDialog, {
      props: {
        shortcuts: [
          { key: 'F', description: 'Pay Respects', long: false },
          { key: 'Space', description: 'Jump', long: true },
        ],
      },
    })

    // show the dialog
    await wrapper.find('button').trigger('click')

    expect(document.body.getElementsByTagName('kbd').length).toBe(2)
  })
})
