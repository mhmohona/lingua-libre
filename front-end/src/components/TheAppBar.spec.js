import { describe, it, expect } from 'vitest'
import { mount, RouterLinkStub } from '@vue/test-utils'
import TheAppBar from './TheAppBar.vue'

describe('rendering tests', () => {
  it('should have all links inside a <nav>', () => {
    const wrapper = mount(TheAppBar, {
      global: {
        stubs: {
          RouterLink: RouterLinkStub,
        },
      },
    })

    // router-link is expected to be turned into a <a> tag
    expect(wrapper.find('nav a').exists()).toBeTruthy()

    expect(wrapper.findAll('a').length).toEqual(wrapper.findAll('nav a').length)
  })

  // Test if it has links to homepage and the RecordWizard
  // These are the two I think are absolutely mandatory, hence why they are explicitely required by these two tests
  // - Poslovitch
  it('should have a link to the homepage (/)', () => {
    const wrapper = mount(TheAppBar, {
      global: {
        stubs: {
          RouterLink: RouterLinkStub,
        },
      },
    })

    expect(
      wrapper
        .findAllComponents(RouterLinkStub)
        .find((routerLink) => routerLink.props('to') === '/') !== undefined,
    ).toBeTruthy()
  })

  it('should have a link to the Record Wizard (/record)', () => {
    const wrapper = mount(TheAppBar, {
      global: {
        stubs: {
          RouterLink: RouterLinkStub,
        },
      },
    })

    expect(
      wrapper
        .findAllComponents(RouterLinkStub)
        .find((routerLink) => routerLink.props('to') === '/record') !== undefined,
    ).toBeTruthy()
  })
})
