import { describe, it, expect } from 'vitest'
import { shallowMount } from '@vue/test-utils'
import TheFooter from './TheFooter.vue'

describe('rendering tests', () => {
  it('should have all links inside a <footer>', () => {
    const wrapper = shallowMount(TheFooter)

    expect(wrapper.find('footer a').exists()).toBeTruthy()

    expect(wrapper.findAll('a').length).toEqual(wrapper.findAll('footer a').length)
  })
})
